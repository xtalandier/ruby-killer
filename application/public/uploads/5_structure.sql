SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `rubykiller` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci ;
USE `rubykiller` ;

-- -----------------------------------------------------
-- Table `rubykiller`.`users`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `rubykiller`.`users` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `nickname` VARCHAR(45) NULL ,
  `email` VARCHAR(45) NULL ,
  `password` VARCHAR(32) NULL ,
  `etre` VARCHAR(45) NULL ,
  `confirmed` TINYINT(1) UNSIGNED NULL DEFAULT 0 ,
  `blocked` TINYINT(1) UNSIGNED NULL DEFAULT 0 ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `rubykiller`.`keywords`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `rubykiller`.`keywords` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `keyword` VARCHAR(100) NULL ,
  `user_id` INT UNSIGNED NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_user_motcle_idx` (`user_id` ASC) ,
  CONSTRAINT `fk_user_motcle`
    FOREIGN KEY (`user_id` )
    REFERENCES `rubykiller`.`users` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `rubykiller`.`invitation`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `rubykiller`.`invitation` (
  `id_invitation` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `id_user1` INT UNSIGNED NULL ,
  `id_user2` INT UNSIGNED NULL ,
  `message` TEXT NULL ,
  `date` DATE NULL ,
  `type` VARCHAR(45) NULL ,
  PRIMARY KEY (`id_invitation`) ,
  INDEX `fk_invit_user1_idx` (`id_user1` ASC) ,
  INDEX `fk_invit_user2_idx` (`id_user2` ASC) ,
  CONSTRAINT `fk_invit_user1`
    FOREIGN KEY (`id_user1` )
    REFERENCES `rubykiller`.`users` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_invit_user2`
    FOREIGN KEY (`id_user2` )
    REFERENCES `rubykiller`.`users` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `rubykiller`.`friends`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `rubykiller`.`friends` (
  `user_id_1` INT UNSIGNED NOT NULL ,
  `user_id_2` INT UNSIGNED NOT NULL ,
  INDEX `fk_ami_user1_idx` (`user_id_1` ASC) ,
  INDEX `fk_ami_user2_idx` (`user_id_2` ASC) ,
  CONSTRAINT `fk_ami_user1`
    FOREIGN KEY (`user_id_1` )
    REFERENCES `rubykiller`.`users` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ami_user2`
    FOREIGN KEY (`user_id_2` )
    REFERENCES `rubykiller`.`users` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `rubykiller`.`images`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `rubykiller`.`images` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `user_id` INT UNSIGNED NOT NULL ,
  `name` VARCHAR(256) NULL COMMENT 'Contient le nom.extension\\n\\nSera enregistrer physiquement selon ce nommage :\\nid_nom.extension' ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_upload_user_id_idx` (`user_id` ASC) ,
  CONSTRAINT `fk_upload_user_id`
    FOREIGN KEY (`user_id` )
    REFERENCES `rubykiller`.`users` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
