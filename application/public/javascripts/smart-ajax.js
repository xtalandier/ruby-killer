/*
 * Smart Ajax - Static
 * Author : Jean-Hugues Gilbert
 */

var SmartAjax = {
	Fill : function(obj, url) {
		$.ajax({
			url: url,
			beforeSend: function () {
				$(obj).fadeOut(100);
			},
			success: function(html) { 
				$(obj).empty().html(html).fadeIn(200);
			  }
		  });
	}
}