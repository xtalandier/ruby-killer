/* 
 * @author Jean-Hugues Gilbert
 */

var Mailing = function() {
    var $this = this; // tricky trick

    this.mailBar = null;

    this.id_user_session = -1;
    this.nickname_session = "";

    this.convers = [];
    this.actualConvers = -1;

    this.timer = null;
    this.majTime = 20; // ms


    this.init = function(id, nick, toOpen) {
        $this.id_user_session = id;
        $this.nickname_session = nick;

        for(var i = 0; i < toOpen.length; i++) {
            this.createDialog(toOpen[i]);
        }
    };

    this.relaunchUpdate = function() {
        clearInterval($this.timer); // au cas où
        $this.timer = setInterval($this.update(), $this.majTime);
    };

    this.getHistory = function(cv) {
        if(cv.isRegistered()) {
            jQuery.ajax({
                type: 'POST',
                url: 'mailing/retreive_history',
                data : {
                   to_user : cv.user
                },
                cache: false
            }).done(function(json) {
                if($this.isOK(json)) {
                    cv.addJSONMessage(json);
                    if($this.timer == null)
                        $this.relaunchUpdate(); // on commence les majs quand on a chopé l'history
                }
            });
        }
    }

    this.update = function() {
        var cv = $this.nextConvers();

        if(cv.isRegistered()) {
            clearInterval($this.timer);
            jQuery.ajax({
                type: 'POST',
                url: 'mailing/retreive',
                data : {
                   id_msg: cv.getLastId(),
                   id_user_distant : cv.user
                },
                cache: false
            }).done(function(json) {
                if($this.isOK(json)) {
                    if(cv.display == false)
                        cv.setDisplay(true);
                    cv.addJSONMessage(json);
                }

                $this.relaunchUpdate();
            });
        }
    }

    this.submit = function(cv) {
        var json = "lol";
        if(cv.isRegistered()) { // safer

            //clearInterval($this.timer);

            var msg = new Message();
            msg.init(cv.user, $this.id_user_session, cv.getContent());

            json = msg.getJSON();

            jQuery.ajax({
                type: 'POST',
                url: 'mailing/send_mail',
                data : {
                   json: json
                },
                cache: false
            }).done(function(html) {
                if($this.isOK(html)) {
                    var split = html.split(',');

                    msg.id = parseInt(split[0]);
                    msg.timestamp = (new Date()).getTime();
                    cv.emptyContent();
                    cv.addMsg(msg)

                    //$this.relaunchUpdate();
                }
            });
        }
    };

    this.nextConvers = function() {
        if($this.convers.length > 0) {
            $this.actualConvers++;

            if($this.actualConvers >= $this.convers.length)
                $this.actualConvers = 0;
        }

        return $this.convers[$this.actualConvers];
    };

    this.createDialog = function(with_user) {
        var cv = new DialogConversation();

        $this.mailBar = $(".mail-bar");

        $this.loadInfos(with_user, function(name) {
            cv.nickname = name;

            cv.init($this);
            cv.register(with_user);
            
            $this.convers.push(cv);

            $this.mailBar.append(cv.tag);

            cv.bindInput();
        });
    }

    this.openDialog = function(id) {
        for(var i = 0; i < $this.convers.length; i++) {
            if(id == $this.convers[i].user) {
                $this.convers[i].setDisplay(true);
            }
        }
    }

    this.closeDialog = function(id) {
        for(var i = 0; i < $this.convers.length; i++) {
            if(id == $this.convers[i].user) {
                $this.convers[i].setDisplay(false);
            }
        }
    }

    this.loadInfos = function(id, fn) {
            jQuery.ajax({
                type: 'POST',
                url: 'user/get_name_by_id',
                data : {
                   id: id
                },
                cache: false
            }).done(function(html) {
                if($this.isOK(html)) {
                    fn(html);
                }
            });
    };

    this.isOK = function(txt) {
        var bRet = false;
        if(txt != null && txt != "error" && txt != "[]") {
            bRet = true;
        }

        return bRet;
    }

};

var DialogConversation = function () {
    var $thisD = this;
    this.user = -1;
    this.nickname = null;
    this.registered = false;
    this.tag = null;
    this.content = null;
    this.inner   = null;
    this.header  = null;
    this.submit  = null;
    this.messages = [];
    this.engine = null;

    this.display = false;

    this.lastIdMess = -1;

    this.init = function(_eng) {
        $thisD.engine = _eng;
    };

    // register all the component to work correctly
    this.register = function(with_user) {
        $thisD.user = with_user;

        $thisD.registered = true;
        $thisD.generate();

        // register the submit button
        $("#SUBT"+$thisD.user).live('click',function() {
            $thisD.engine.submit($thisD);
        });

        $thisD.engine.getHistory($thisD);
    };

    this.setDisplay = function(display) {
        if(display)
            $('#'+$thisD.tag.id).fadeIn(100);
        else
            $('#'+$thisD.tag.id).fadeOut(100);

        this.display = display;
    }

    this.addMsg = function(msg) {
        $thisD.messages.push(msg);
        var d = new Date(msg.timestamp);

        if(msg.from_user != $thisD.lastIdMess) {
            var name = "";
            if(msg.from_user == $thisD.user)
                name = $thisD.nickname; // the conversation user
            else
                name = $thisD.engine.nickname_session; // my user

            $('#INNER'+$thisD.user).append("<div class=\"mail-line mail-header-line\">" + name  + "</div>");
        }

        $thisD.lastIdMess = msg.from_user;

        $('#INNER'+$thisD.user).append("<div class=\"mail-line\">" + msg.content  + "<div class=\"mail-time pull-right\">"+ d.toLocaleTimeString() +"</div></div>");
    };

    this.generate = function() {
        var header = document.createElement('div');
        header.setAttribute('class', 'mail-header');
        header.setAttribute('onmousedown', 'javascript:mEngine.closeDialog('+$thisD.user+');');
        header.innerHTML = $thisD.nickname;

        $thisD.header = header;

        var inner = document.createElement('div');
        inner.setAttribute('class', 'mail-inner');
        inner.setAttribute('id', 'INNER'+$thisD.user);
        inner.innertHTML = 'Pas de messages.';

        $thisD.inner = inner;

        var input = document.createElement('div');
        input.setAttribute('class', 'mail-input input-append');

        var inputText = document.createElement('input');
        inputText.setAttribute('type', 'text');
        inputText.setAttribute('id', 'content'+$thisD.user);
        inputText.setAttribute('placeholder', 'Type your text here ...');

        $thisD.content = inputText;

        var submitButton = document.createElement('button');
        submitButton.setAttribute('class', 'btn btn-danger');
        submitButton.setAttribute('id', 'SUBT'+$thisD.user);
        submitButton.innerHTML = 'Go';

        $thisD.submit = submitButton;

        $thisD.tag = document.createElement('div');
        $thisD.tag.setAttribute('class', 'mail-convers pull-right');
        $thisD.tag.setAttribute('id', 'CONV'+this.user);

        input.appendChild(inputText);
        input.appendChild(submitButton);

        $thisD.tag.appendChild(header);
        $thisD.tag.appendChild(inner);
        $thisD.tag.appendChild(input);
    };


    this.isRegistered = function() { // check if all is correct
        var ret = false;

        if($thisD.registered == true && $thisD.inner != null && $thisD.content != null && $thisD.nickname != null) {
            ret = true;
        }

        return ret;
    };


    this.addJSONMessage = function(_json) {
        json = jQuery.parseJSON(_json);

        $.each(json, function(index, msg) {
            var tmpMsg = new Message();

            tmpMsg.id = msg["message"]["id"];
            tmpMsg.from_user = msg["message"]["user_id_emet"];
            tmpMsg.to_user = msg["message"]["user_id_dest"];
            tmpMsg.content = msg["message"]["message"];
            tmpMsg.timestamp =  msg["message"]["created_on"];

            $thisD.messages.push(tmpMsg);
            $thisD.addMsg(tmpMsg);
        });

    };

    this.getLastId = function() {
        id = -1;
        if($thisD.messages.length > 0)
            id = $thisD.messages[$thisD.messages.length - 1].id;


        return id;
    };

    this.getLastDate = function() {
        ts = 0;
        if($thisD.messages.length > 0)
            ts = $thisD.messages[$thisD.messages.length - 1].timestamp;


        return ts;
    };

    this.getContent = function() {
        return $($thisD.content).val();
    };

    this.emptyContent = function() {
        $('#'+$thisD.content.id).val('');
    };

    this.bindInput = function() {
        $('#'+$thisD.content.id).keydown(function(e) {
            if(e.keyCode == 13) // enter
                $thisD.engine.submit($thisD);
        });
    };
}

var Message = function() {
    $thisM = this;
    this.id      = -1;
    this.from_user = -1;
    this.to_user = -1;
    this.content   = "";
    this.timestamp = -1;

    this.init = function(_to_user, _from_user, _content) {
        $thisM.to_user = _to_user;
        $thisM.from_user = _from_user
        $thisM.content   =  _content;
    }
/*
    this.parseJSON = function(json) {
        json = jQuery.parseJSON(json);

        $thisM.id = json["id"];
        $thisM.to_user = json["to_user"];
        $thisM.content = json["content"];
    };
*/
    this.getJSON = function() {
        var strRet = "";
        var tab    = {
            "from_user" : $thisM.from_user,
            "to_user" : $thisM.to_user,
            "content"   : $thisM.content
        };

        return JSON.stringify(tab);
    }
};