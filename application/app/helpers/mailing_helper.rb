module MailingHelper
  def get_messages_of(id)
    messages = Message.get_all_messages_of id

    messages
  end

  def get_messages_of_with(id1, id2)
    if id1 != id2 then
      messages = Message.get_all_messages_of_with id1, id2

      messages
    end
  end

  def get_users_messages_of(id)
    if id == -1 then
      id = session[:user].id
    end

    related_users = Message.get_all_messages_of id

    users = Array.new
    related_users.each do |m|
      if m.me_user.id != session[:user].id then
        users.push m.me_user
      end
      if m.dest_user.id != session[:user].id then
        users.push m.dest_user
      end
    end

    users
  end

  def generate_mailing_button(id)
    str = '<div class="pull-right"><button onmousedown="mEngine.openDialog(' + id.to_s + ');"><i class="icon-envelope"></i></button></div>'

    str
  end
end
