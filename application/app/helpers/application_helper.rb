# Methods added to this helper will be available to all templates in the application.
module ApplicationHelper
  def verify_security_token
    puts "Run"
  end

  def getMyName
    session[:user].nil? ? "" : session[:user].nickname
  end

  def get_friends
    @allfriends = User.reload
  end

  def getJSFriends
    str = '[';
    friends = get_friends
    cnt = friends.occurances_as_me_user.count
    i = 0

    friends.occurances_as_me_user.each do |f|
      str += f.friend_user.id.to_s;
      unless cnt == i then
        str += ','
      end

      i += 1
    end

    str += ']'

    str
  end

  def get_nb_friends
    session[:user].nil? ? 0 : session[:user].occurances_as_friend_user.count
  end
  
  def get_nb_invitations
    session[:user].nil? ? 0 : session[:user].occurances_as_friend_invitation.count
  end


end
