class InscriptionController < ApplicationController
  layout 'simple'
  
  def inscription
  end

  def index
    redirect_to :action => "form"
  end

  def form
    @user = flash["user"].nil? ? User.new : flash["user"]
  end

  # New user
  def new
    @user = User.new
    @user.nickname = params[:user][:nickname]
    @user.email = params[:user][:email]
    @user.password = params[:user][:password]
    @user.etre = params[:user][:etre]
    @user.save
    flash["message"] = "Client cree"
    flash["user"] = @user
    unless @user.errors.count == 0
      redirect_to :action => "form"
    else
      Notifier.sendemail('noreply@talandier.fr', @user.email , 'Confirmation', render_to_string('/email/hello'))
    end
  end
  
  # Mail Confirmation
  def confirm
    hash = params[:id]
    @user = User.getfromhash(hash)
    unless @user.nil?
      @user.confirmed = 1
      @user.save
    end
  end

  def desinscription
    @u = User.getfromhash params[:id]
    if @u.nil?
      @message = "Qui etes vous ?"
    else
      @message = "Vous etes desinscrit"
      @u.destroy
    end
  end
end
