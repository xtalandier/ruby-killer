class MainController < ApplicationController
  layout 'simple'
  def main
    @user = nil
    unless session[:user].nil?
      redirect_to :controller => 'rechercher' , :action => 'afficher'
    end
  end
end
