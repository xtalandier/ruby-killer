class AdminController < ApplicationController
  def index
    @users = User.find :all
  end

  def lock
    user = User.find_by_id params[:id]
    user.blocked = 1
    user.save
    redirect_to :controller => "amis", :action => "profil" , :id => user.id
  end

  def unlock
    user = User.find_by_id params[:id]
    user.blocked = 0
    user.save
    redirect_to :controller => "amis", :action => "profil" , :id => user.id
  end

  def confirm
    user = User.find_by_id params[:id]
    user.confirmed = 1
    user.save
    redirect_to :controller => "amis", :action => "profil" , :id => user.id
  end
  def unconfirm
    user = User.find_by_id params[:id]
    user.confirmed = 0
    user.save
    redirect_to :controller => "amis", :action => "profil" , :id => user.id
  end

  def kill
    user = User.find_by_id params[:id]
    user.destroy
    redirect_to :controller => "amis", :action => "afficher" , :id => user.id
  end

end
