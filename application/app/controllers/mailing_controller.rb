class MailingController < ApplicationController
  def mailing
    @mail = true
    @mail_id_to = params[:id] # correspondant
  end

  # view
  def read
    @id = params[:id].nil? ? 0 : params[:id]
    if @id == '0'
      @id = User.find(:first, :conditions => {:etre => 'admin'}).id
    end
    @messages = Message.get_all_messages_of_with session[:user].id, @id
    @friends_all = Friend.get_all_of session[:user].id
  end

  def retreive
    str = 'error'
    id_msg = params[:id_msg]
    from_user = params[:id_user_distant]
    to_user   = session[:user].id
    json = nil

    if from_user != to_user then

      unless id_msg == -1
        messages = Message.find :all, :conditions => 'id > ' + id_msg.to_s + ' AND user_id_dest = ' + to_user.to_s + ' AND user_id_emet = ' + from_user.to_s, :order => 'id'
      else
        messages = Message.find :all, :conditions => 'user_id_dest = ' + to_user.to_s + ' AND user_id_emet = ' + from_user.to_s, :order => 'id', :limit => '1'
      end
      
      unless messages.nil?
        json = ActiveSupport::JSON.encode(messages)

        unless json.nil?
          str = json
        end
      end
    end

    render:text => str
  end

  def retreive_history
    from_user = session[:user].id
    to_user   = params[:to_user]
    json = nil
    messages = Message.find :all, :conditions => '(user_id_dest = ' + from_user.to_s + ' AND user_id_emet = ' + to_user.to_s + ') OR (user_id_dest = ' + to_user.to_s + ' AND user_id_emet = ' + from_user.to_s + ')', :order => 'id DESC', :limit => '1'

    unless messages.nil?
      json = ActiveSupport::JSON.encode(messages)

      unless json.nil?
        render:text => json
      end
    end
  end

  def readed
    msg = Message.find_by_id(params[:id])

    msg.readed = 1
    if !msg.save then
      render:text => 'error'
    end
  end

  def send_mail
    ret = ""
    json = nil
    unless(params[:json].nil?)
      json = ActiveSupport::JSON.decode(params[:json])
    end

    unless json.nil?
      mail = Message.new

      mail.user_id_emet = json["from_user"]
      mail.user_id_dest = json["to_user"]
      mail.message = json["content"]
      mail.created_on = Time.now

      if !mail.save then
        ret = "error"
      else
        ret = Message.get_last_id.to_s
      end
    end

    render:text => ret
  end

  def send_static_mail
      mail = Message.new
      id_to = params["id_to"]
      if id_to == "0"
        id_to = User.find(:first, :conditions => {:etre => 'admin'}).id
      end
      mail.user_id_emet = params["id_from"]
      mail.user_id_dest = id_to
      mail.message = params["content"]
      mail.created_on = Time.now

      mail.save


    redirect_to :action => 'read', :id => params["id_to"]
  end
end
