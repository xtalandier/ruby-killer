class ProfilController < ApplicationController
  def profil
    @user = session[:user]
    @images = Image.find_by_user_id @user.id, :order => "id DESC"
  end

  def form
    @user   = session[:user]
    @images = Image.find_by_user_id @user.id, :order => "id DESC"
    @errors = Array.new
    @keywords = Keyword.find :all , :group => "keyword"

    i = 0;

    # Code de gros cochons
    unless flash[:img].nil?
      i = 0
      flash[:img].errors.each do |attribute, errors_array|
        errors_array.each do |e|
          @errors[i] = e.to_s
          i += 1
        end
      end
    end

    unless flash[:user].nil?
      i = 0
      flash[:user].errors.each do |attribute, errors_array|
        errors_array.each do |e|
          @errors[i] = e.to_s
          i += 1
        end
      end
    end

  end

  def save

    img = nil

    # upload
    unless params[:upload].nil?
      img = Image.new
      img.user_id = session[:user].id
      img.name    = ""

      # we save it first to generate an id
      img.save

      # we load it again do update it
      img = Image.find_by_id Image.get_last_id

      result = Image.save(params[:upload], img.id)

      unless(result.nil?)
        img.name    = result
        img.save
      else
        img.destroy
      end
    end

    # user data
    user = User.find_by_id session[:user].id

    unless user.nil?
      user.email = params[:email]
      unless params[:passwd].empty?
        user.password = params[:passwd]
      end
      unless params[:tagline].empty?
        user.tagline = params[:tagline]
      end
      unless params[:descript].empty?
        user.description = params[:descript]
      end

      user.save
    end

    #todo tags

    flash[:img] = img
    flash[:user] = user

    redirect_to :action => "form"
  end


  def tagremove
    begin
      @keyword = Keyword.find_by_id params[:id]
      @keyword.destroy
      render :text => "ok"
    rescue
      render :text => "nok"
    end
  end

  def tagadd
    keyword = Keyword.new
    keyword.keyword = params[:tag]
    keyword.user_id = session[:user].id
    keyword.save
    render :text => keyword.to_json
  end
end