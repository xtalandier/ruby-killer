# Filters added to this controller apply to all controllers in the application.
# Likewise, all the methods added will be available for all controllers.

class ApplicationController < ActionController::Base

  helper :all # include all helpers, all the time
  protect_from_forgery # See ActionController::RequestForgeryProtection for details

  # Appelé avant les actions
  before_filter :check_perms

  # Appelé avant les actions
  def check_perms
    # Recharge l'utilisateur
    if !session[:user].nil?
      puts "*******RELOAD USER"
      session[:user] = User.reload_with_id session[:user].id
    end
    # Si l'utilisateur n'est pas connecté,
    # alors il doit être vers le formulaie de
    # connexion || d'inscription
    is_logged = !session[:user].nil?
    is_public = controller_path.to_s == "user" || controller_path.to_s == "main"  || controller_path.to_s == "inscription"
    puts "****************--------" + is_public.to_s + "   <=" + (controller_path.to_s == "user").to_s + "," + (controller_path.to_s == "main").to_s + "," + (controller_path.to_s == "inscription").to_s
    if !is_logged and !is_public
      redirect_to :controller => "main" , :action => "main"
    end
  end

around_filter :you_dont_have_bloody_clue

  protected

  def you_dont_have_bloody_clue
    klasses = [ActiveRecord::Base, ActiveRecord::Base.class]
    methods = ["session", "cookies", "params", "request"]

    methods.each do |shenanigan|
      oops = instance_variable_get(:"@_#{shenanigan}")

      klasses.each do |klass|
        klass.send(:define_method, shenanigan, proc { oops })
      end
    end

    yield

    methods.each do |shenanigan|
      klasses.each do |klass|
        klass.send :remove_method, shenanigan
      end
    end
  end

end
