class InvitationController < ApplicationController
  def invitation
  end

  def inviter
    invitation = Invitation.new
    invitation.user_id_1     = session[:user].id
    invitation.user_id_2     = params[:id]
    invitation.message       = params[:invitation][:message]
    invitation.is_invitation = 1
    invitation.created_on    = DateTime.now
    invitation.save
    redirect_to :controller => "amis" , :action => "profil" , :id => params[:id]
  end

  def afficher
    @invitations = Invitation.find :all , :conditions => {:user_id_2 => session[:user].id}
  end

  def accepter
    inv = Invitation.find_by_id params[:id]
    id = inv.user_id_1
    id_user = session[:user].id
    f1 = Friend.new
    f1.user_id_1 = id_user
    f1.user_id_2 = id
    f1.save
    f2 = Friend.new
    f2.user_id_1 = id
    f2.user_id_2 = id_user
    f2.save
    inv.destroy
    redirect_to :action => "afficher"
  end

  def refuser
    inv = Invitation.find_by_id params[:id]
    inv.destroy
    redirect_to :action => "afficher"
  end
end
