class UserController < ApplicationController
  layout 'simple'
  def loginform
    @user = flash["user"].nil? ? User.new : flash["user"]

    @err =  flash[:err]
  end

  def login
    @user = User.connexion(params[:user][:email], params[:user][:password])
    if @user.nil?
      flash[:err] = "Erreur dans l'email/password"
      redirect_to :action => "loginform"
    elsif !@user.confirmed or @user.blocked == 1
      flash[:err] = "Compte pas actif ou bloque"
      redirect_to :action => "loginform"
    elsif @user
      session[:user] = @user
      redirect_to :controller => "rechercher" , :action => "afficher"
    else
      redirect_to :action => "loginform"
    end
  end

  def get_name_by_id
    render:text => User.get_name_by_id(params[:id])
  end

  def logout
      session[:user] = nil
      redirect_to :controller => "main" , :action => "main"
  end
end
