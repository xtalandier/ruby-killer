class RechercherController < ApplicationController
  def afficher
    sql = "SELECT u.* FROM users u
                                LEFT JOIN keywords w ON (w.user_id = u.id)"
    sql1 = ''
    sql2 = ''
    
    unless params[:query].nil?
      sql2 = ""
      if params[:query][:killer] != "0"
        sql2+= " etre = 'killer'"
      end
      if params[:query][:client] != "0"
        sql2+= (sql2.blank? ? "" : " OR ") + " etre = 'client'"
      end
      sql1 = ""
      if params[:query][:val].blank? and sql2.blank?
        sql1 = ""
      else
        flash[:query] = params[:query][:val]
        splits = params[:query][:val].split(/ /)
        splits.each do |val|
          puts val
          sql1+= " w.keyword LIKE " + "'%" + val + "%' OR"
        end
        sql1 = sql1[0..(sql1.length - 3)]
      end
    end
    if not sql1.blank? or not sql2.blank?
      sql += " WHERE  "
    end
    sql+= sql1 + ((sql2.blank? or sql1.blank?) ? "" : " AND ") + sql2
    sql+= " GROUP BY u.id"
    puts sql
    @users = User.find_by_sql sql
  end

end
