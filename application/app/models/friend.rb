class Friend < ActiveRecord::Base
  belongs_to :me_user     , :class_name => "User", :foreign_key => "user_id_1"
  belongs_to :friend_user , :class_name => "User", :foreign_key => "user_id_2"


  def self.get_all_of(id)
    friend_of = Array.new
    tmp_bool = true
    
    friends = find :all, :conditions => {:user_id_1 => id}
    friends.each do |f|
      tmp_bool = true
      friend_of.each do |f1|
        unless f.id == f1.id
          tmp_bool = false
        end
      end

      if tmp_bool
        friend_of.push f.friend_user
      end
    end

    friends = find :all, :conditions => {:user_id_2 => id}
    friends.each do |f|
      tmp_bool = true
      friend_of.each do |f1|
        unless f.id == f1.id
          tmp_bool = false
        end
      end

      if tmp_bool
        friend_of.push f.me_user
      end
    end

    friend_of
  end
end
