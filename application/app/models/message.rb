class Message < ActiveRecord::Base
  validates_presence_of :message , :user_id_emet , :user_id_dest , :created_on

  belongs_to :me_user     , :class_name => "User", :foreign_key => "user_id_emet"
  belongs_to :dest_user    , :class_name => "User", :foreign_key => "user_id_dest"

  def self.get_last_id
    lastid = Message.find :all, :order => "id DESC"
    new_id = 0

    unless lastid.nil?
      new_id = lastid[0].id
    end

    new_id
  end

  def self.get_all_messages_of(id)
      messages = Message.find :all,
      :conditions => "user_id_dest = " + id.to_s + " OR user_id_emet = " + id.to_s,
      :order => "id",
      :group => "user_id_dest AND user_id_emet"

    messages
  end

  def self.get_all_messages_of_with(id1, id2)
    messages = Message.find :all,
      :conditions => "(user_id_dest = " + id1.to_s + " AND user_id_emet = " + id2.to_s + ") OR (user_id_dest = " + id2.to_s + " AND user_id_emet = " + id1.to_s + ")",
      :order => "id"

    messages
  end


end
