require 'digest/md5'
class User < ActiveRecord::Base
  validates_presence_of :email , :nickname , :password , :etre
  validates_uniqueness_of  :nickname , :email
  has_many :keywords
  has_many :images

  has_many  :occurances_as_me_user     , :class_name => "Friend", :foreign_key => "user_id_1"
  has_many  :occurances_as_friend_user , :class_name => "Friend", :foreign_key => "user_id_2"

  has_many  :occurances_as_me_invitation     , :class_name => "Invitation", :foreign_key => "user_id_1"
  has_many  :occurances_as_friend_invitation , :class_name => "Invitation", :foreign_key => "user_id_2"

  def validate
    unless email.blank?
      unless email =~ /^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$/
          errors.add(:email, "Your email address does not appear to be valid")
      end
    end
  end
  def self.connexion(email, password)
    user = find_by_email(email)
    if user && user.password == password
        user
    else
      nil
    end
  end

  def gethash
    Digest::MD5.hexdigest(self.email + self.id.to_s)
  end

  def get_confirm_url
    'http://' + request.host + ':' + request.port.to_s + '/inscription/confirm/' + gethash
  end

  def self.getfromhash(hash)
    u = User.find_by_sql ["SELECT * FROM users WHERE MD5(CONCAT(email , id)) = ? " , hash]
    if u.count == 0
      nil
    else
      u[0]
    end
  end
  
  def self.get_name_by_id(id)
    nickn = nil

    if id.to_i != 0
      user = User.find_by_id(id)
      nickn = user.nickname.to_s
    else
      nickn = "Administrateur"
    end

    nickn
  end

  def self.reload
     find_by_id session[:user].id
  end

  def self.reload_with_id(id)
     find_by_id id
  end

  def is_invited_by_me
    inv    = Invitation.find :all , :conditions => {:user_id_1 => session[:user].id , :user_id_2 => id}
    inv.count > 0
  end

  def self.get_all
    users = User.find :all

    users
  end

  def is_my_friend
    friend = Friend.find :all , :conditions => {:user_id_1 => session[:user].id , :user_id_2 => id}
    friend.count > 0
  end
end
