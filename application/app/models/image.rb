class Image < ActiveRecord::Base
  def validate

  end

  def self.get_last_id
    lastid = Image.find :all, :order => "id DESC"
    new_id = 0

    unless lastid.nil?
      new_id = lastid[0].id
    end
    
    new_id
  end

  def self.get_path(img)
    strReturn = nil
    unless img.nil?
      strReturn = "uploads/" + img.id.to_s + "_" + img.name
    end

    strReturn
  end

  def self.save(upload, id)
    #create the awesome struct

    name =  upload['datafile'].original_filename

    just_filename = File.basename(name)
    just_filename.sub(/[^\w\.\-]/,'_') # replace unauthorized character

    filename = id.to_s + "_" + just_filename

    directory = "public/uploads"

    # create the file path
    path = File.join(directory, filename)

    # write the file
    unless File.open(path, "wb") { |f| f.write(upload['datafile'].read) } then
      filename = nil
    end

    just_filename #return the file name
  end
  def gethtmlurl
    "/uploads/" + id.to_s + "_" + name
  end
end
