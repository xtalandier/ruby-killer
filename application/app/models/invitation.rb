class Invitation < ActiveRecord::Base
  belongs_to :me_user     , :class_name => "User", :foreign_key => "user_id_1"
  belongs_to :friend_user , :class_name => "User", :foreign_key => "user_id_2"
end
