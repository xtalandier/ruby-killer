require 'action_mailer'
class Notifier
  def self.sendemail(from , to , subject , content)
    require 'net/http'
    # get the url that we need to post to
    url = URI.parse('http://xavier.talandier.fr/sendmail.php')
    # build the params string
    post_args1 = {
      :from => from ,
      :to => to,
      :subject => subject,
      :content => content
    }
    resp, data = Net::HTTP.post_form(url, post_args1)
    puts "+++++++++++++++++++++++++"
    puts data
    puts "+++++++++++++++++++++++++"
  end
end