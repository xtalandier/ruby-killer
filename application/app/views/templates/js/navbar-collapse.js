/*
 * Navbar Collapse script
 * Author : Jean-Hugues Gilbert
 */

var Collapse = function() {
	var $this = this;
	this.height = 300;
	this.active = false;
	
	this.Register = function(e) {
		$(e).click(function() {
			var str = $this.height + "px";
			
			if($this.active)
				str = "0px";
			
			$(".navbar-collapse").animate({
				height:str
			}, 500);
		
			$this.active = !$this.active;
		});
		
	}
};

var _Col = new Collapse();