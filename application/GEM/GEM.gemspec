# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)
require "GEM/version"

Gem::Specification.new do |s|
  s.name        = "GEM"
  s.version     = Gem::VERSION
  s.authors     = ["Xavier TALANDIER"]
  s.email       = ["xavier@talandier.fr"]
  s.homepage    = ""
  s.summary     = %q{TODO: Write a gem summary}
  s.description = %q{TODO: Write a gem description}

  s.rubyforge_project = "GEM"

  s.files         = `git ls-files`.split("\n")
  s.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.require_paths = ["lib"]
end
