# To change this template, choose Tools | Templates
# and open the template in the editor.

class TThumbBox < BaseTemplate
  def initialize (label, hash)
    @label = label
    super(hash)
  end

  # some improvements to do (add a link)
  def prepare
    @html = "<div class=\"thumb\">"
    @html += "<img class=\"thumb-img\" src=\"" + @object[:img_src] + "\" />"
    @html += "<div class=\"smartreader\">"
    @html += "<h5>"+ @object[:title] +"</h5>"
    @html += @object[:content]
    @html += "</div>"
    @html += "</div>"
  end

  def html
    CGI::unescapeHTML(@html)
  end
end
