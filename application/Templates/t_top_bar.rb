# To change this template, choose Tools | Templates
# and open the template in the editor.

class TTopBar < BaseTemplate
  def initialize object
    @object = object
  end

  def prepare
    @html = "<div class=\"navbar navbar-fixed-top\">"
    @html += "<div class=\"navbar-collapse\">
                <div class=\"navbar-collapse-inner\">
                  <div class=\"navbar-side-left\">
                    <div>
                      <div style=\"display: inline-block;\">"
    @html += "<img src=\""+ @object[:client][:src_avatar] +"\" style=\"width: 92px; height: 92px;\" >"
    @html += "</div>"
    @html += "<div style=\"display: inline-block; font-weight: bold; vertical-align: top; padding: 5px;\">"
		@html += @object[:client][:first_name] + " " + @object[:client][:last_name] + "<br>"
    @html += @object[:client][:statut]
		@html += "</div>"
    @html += "</div>"
    @html += "<div>"
						<ul class="tower-choice">
							<li onmouseup="SmartAjax.Fill($('.navbar-side-right'), 'params/params.include.html');">Paramètres</li>
							<li>Amis</li>
							<li>Messages</li>
							<li>Notifications</li>
						</ul>
					</div>
				</div>
				<div class="navbar-side-right">
					<div>lol</div>
				</div>
			</div>
		</div>
		<div class="navbar-inner">
			<a id="navbar-logo" class="brand" style="margin:0; padding: 0; margin-top: 4px; margin-left: 4px;" href="#"><img src="img/logo.png"></a>
				<ul class="nav">
					<li><a href="#">Home</a></li>
					<li><a href="#">Link</a></li>
					<li><a href="#">Link</a></li>
				</ul>

				<a class="btn btn-danger pull-right navbar-collapser" style="margin-right: 5px;" href="#">Profil</a>
		</div>
	  </div>
  end

  def html
    
  end

private
  def prepareCollapse


  end
end
