# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_RubyKillerV2_session',
  :secret      => '1aa2d79a0cf4a65c7b0b8252e86c888b62c53a6c1605b23f755f9a7bb97daebb38a26902461fd9bf7f2881a4d8f6686056757d2c2a1ab2f29ae507d57797f8c0'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
