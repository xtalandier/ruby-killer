require 'test_helper'

class MailerSignupTest < ActionMailer::TestCase
  test "mailersignup" do
    @expected.subject = 'MailerSignup#mailersignup'
    @expected.body    = read_fixture('mailersignup')
    @expected.date    = Time.now

    assert_equal @expected.encoded, MailerSignup.create_mailersignup(@expected.date).encoded
  end

end
