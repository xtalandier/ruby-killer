require 'test_helper'

class MailerForgotPasswdTest < ActionMailer::TestCase
  test "mailerforgotpasswd" do
    @expected.subject = 'MailerForgotPasswd#mailerforgotpasswd'
    @expected.body    = read_fixture('mailerforgotpasswd')
    @expected.date    = Time.now

    assert_equal @expected.encoded, MailerForgotPasswd.create_mailerforgotpasswd(@expected.date).encoded
  end

end
